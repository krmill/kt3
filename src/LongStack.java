import java.util.*;

public class LongStack {

    private LinkedList<Long> stack;

    public LongStack() {
        stack = new LinkedList<>();
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        LongStack newStack = new LongStack();
        for (Long aLong : stack) {
            newStack.push(aLong);
        }
        return newStack;
    }

    public boolean stEmpty() {
        return stack.size() == 0;
    }

    public void push(long a) {
        stack.add(a);
    }

    public long pop() {
        if (stEmpty()) {
            throw new RuntimeException("Not enough numbers in the stack.");
        }
        return stack.removeLast();
    }

    public void op(String s) {
        long num2 = pop();
        long num1 = pop();
        switch (s.toLowerCase()) {
            case "*":
                push(num1 * num2);
                break;
            case "/":
                push(num1 / num2);
                break;
            case "+":
                push(num1 + num2);
                break;
            case "-":
                push(num1 - num2);
                break;
            case "swap":
                push(num2);
                push(num1);
                break;
            case "rot":
                long firstNum = pop();
                push(num1);
                push(num2);
                push(firstNum);
                break;
            default:
                throw new RuntimeException("Bad operator: " + s);
        }
    }

    public long tos() {
        int size = stack.size();
        if (size > 0) {
            return stack.get(size - 1);
        }
        throw new RuntimeException("Stack is empty.");
    }

    @Override
    public boolean equals(Object o) {
        int size = stack.size();
        if (this == o) {
            return true;
        }
        if (o instanceof LongStack) {
            LongStack other = (LongStack) o;
            if (size == other.stack.size()) {
                for (int i = 0; i < size; i++) {
                    if (!stack.get(i).equals(other.stack.get(i))) {
                        return false;
                    }
                }
                return true;
            }
        }
        return false;
    }

    @Override
    public String toString() {
        return stack.toString();
    }

    private static boolean isOp(String op) {
        switch (op.toLowerCase()) {
            case "*":
            case "/":
            case "+":
            case "-":
            case "rot":
            case "swap":
                return true;
            default:
                return false;
        }
    }

    public static long interpret(String pol) {
        pol = pol.trim();
        String[] arr = pol.split("\\s+");
        int count = arr.length;
        if (count == 0) throw new RuntimeException("Empty expression.");
        LongStack longStack = new LongStack();
        for (String s : arr) {
            try {
                long l = Long.parseLong(s);
                longStack.push(l);
            } catch (NumberFormatException ignored) {
                if (isOp(s)) {
                    try {
                        longStack.op(s);
                    } catch (RuntimeException e) {
                        throw new RuntimeException(String.format("Cannot perform %s in expression: %s", s, pol));
                    }

                } else {
                    throw new RuntimeException(String.format("Illegal symbol %s in expression: %s.", s, pol));
                }
            }
        }
        if (longStack.stack.size() == 1) {
            return longStack.pop();
        } else {
            throw new RuntimeException("Too many numbers in expression: " + pol);
        }
    }

    public static void main(String[] args) {
        LongStack asdf = new LongStack();
        asdf.push(1); asdf.push(2); asdf.push(3);
        System.out.println(LongStack.interpret("15 8 10 rot - swap +"));
    }
}